<?php require_once '../functions.php';  ?>
<?php require_once 'header.php';  ?>

<body class="admin">
    <div class="container-fluid">
        <div class="row mt-5 p-0">
            <div class="col-12 col-md-8 offset-md-2">
                <form action="#" method="post" name="import-form" id="import-form" enctype="multipart/form-data">
                    <div>
                        <label>Choose CSV
                            File</label> <input type="file" name="file" id="file" accept=".csv" required>
                        <button type="submit" id="submit" name="import" class="btn-submit">Import Users</button>

                    </div>

                </form>
                <div id="response"></div>

            </div>
        </div>

    </div>

    <?php require_once 'scripts.php';  ?>
    <script>
        $(function() {
            $(document).on('submit', '#import-form', function() {
                $('#response').text('Updating database...').addClass('alert-info').fadeIn()
                var form = $(this);
                var formdata = false;
                if (window.FormData) {
                    formdata = new FormData(form[0]);
                }

                var formAction = form.attr('action');
                $.ajax({
                    url: 'importusers.php',
                    data: formdata ? formdata : form.serialize(),
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data, textStatus, jqXHR) {
                        // Callback code
                        $('#response').html(data).addClass('alert-info').fadeIn();
                    }
                });



                return false;
            });
        });
    </script>

</body>

</html>