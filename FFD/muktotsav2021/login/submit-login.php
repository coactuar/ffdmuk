<?php
require_once "functions.php";

if (isset($_POST['userEmail'])) {
    $email = '';
    $email_error = '';

    if (empty($_POST['userEmail'])) {
        $email_error = 'Please enter Mobile Number';
    } else {
        //if (!filter_var($_POST['userEmail'], FILTER_VALIDATE_EMAIL)) {
            $mob=filter_var($_POST['userEmail']);
            if (strlen($mob)!=10) {
            $email_error = 'Please enter a valid Mobile Number';
        } else {
            $email = $_POST['userEmail'];
        }
    }

    if ($email_error == '') {
        $user = new User();
        //$user->__set('emailid', $email);
        $user->__set('mobilenum', $email);
        $login = $user->userLogin();
        //var_dump($login);
        $reg_status = $login['status'];
        if ($reg_status == "error") {
            $error = $login['message'];
            $data = array(
                'error' => $error
            );
        } else {
            $data = array(
                'success' => true,
                'email' => $_SESSION['mobilenum']

            );
        }
    } else {
        $data = array(
            'email_error' => $email_error
        );
    }

    echo json_encode($data);
}
